#Solutions for Advent of Code 2017
http://adventofcode.com/2017

There is a directory per day. Within that is a part1.py and part2.py corresponding to the two parts of the question each day.

Much of this code was written under time pressure, with too little sleep, in the wee hours of the morning, and frequently in conjuction with alchohol consumption. Under no circumstances should anyone think this code represents any sort of model for how to write good software.

I blame Herms for all of this.